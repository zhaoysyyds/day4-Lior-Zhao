package com.afs.tdd;

public class Location {
    protected int coordinateX;
    protected int coordinateY;
    protected Direction direction;

    private final int actionX[]={1,0,-1,0};
    private final int actionY[]={0,-1,0,1};
    private final Direction []directionEnum = Direction.values();

    public Location(int coordinateX, int coordinateY, Direction direction) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.direction = direction;
    }

    public int getCoordinateX() {
        return this.coordinateX;
    }

    public int getCoordinateY() {
        return this.coordinateY;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void moveAction(){
        coordinateX+=actionX[direction.getNumber()];
        coordinateY+=actionY[direction.getNumber()];
    }
    public void changeDirection(int trend){
        int index=(direction.getNumber()+trend+4)%4;
        direction=directionEnum[index];
    }
}

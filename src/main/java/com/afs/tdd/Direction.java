package com.afs.tdd;

public enum Direction {
    Earth(0), South(1), West(2), North(3);
    private int number;

    Direction(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}

package com.afs.tdd;

import java.util.Date;
import java.util.List;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }
        if (command.equals(Command.Left)) {
            turnLeft();
        }
        if (command.equals(Command.Right)) {
            turnRight();
        }
    }

    private void turnRight() {
//        switch (location.getDirection()) {
//            case North:
//                location.setDirection(Direction.Earth);
//                break;
//            case South:
//                location.setDirection(Direction.West);
//                break;
//            case Earth:
//                location.setDirection(Direction.South);
//                break;
//            case West:
//                location.setDirection(Direction.North);
//                break;
//        }
        location.changeDirection(1);
    }

    private void turnLeft() {
//        switch (location.getDirection()) {
//            case North:
//                location.setDirection(Direction.West);
//                break;
//            case South:
//                location.setDirection(Direction.Earth);
//                break;
//            case Earth:
//                location.setDirection(Direction.North);
//                break;
//            case West:
//                location.setDirection(Direction.South);
//                break;
//        }
        location.changeDirection(-1);
    }


    private void move() {
        this.location.moveAction();
    }

    public void executeBatchCommands(List<Command> commands) {
        commands.forEach(command -> executeCommand(command));
    }
}
